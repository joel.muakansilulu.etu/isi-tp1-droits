#!/bin/bash


su lambda_b

#affichage de contenu pour vérifier le droit de lecture dans dir_b

echo "Affichage de contenu"
cat dir_b/fichier21 
echo

#supprimer un fichier pour vérifier qu'on en a pas le droit
rm dir_b/fichier22

#créer un fichier pour vérifier le droit d'écriture
touch dir_b/fichier24

#supprimer un fichier créé
rm dir_b/fichier24
echo

#afficher le contenu d'un fichier pour vérifier le droit de lecture dans dir_c
cat dir_c/fichier31
echo
echo "Done !"
