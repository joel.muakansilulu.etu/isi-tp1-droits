#!/bin/bash


su lambda_a

#affichage de contenu pour vérifier le droit de lecture dans dir_a

echo "Affichage de contenu"
cat dir_a/fichier11  
echo

#supprimer un fichier pour vérifier qu'on en a pas le droit
rm dir_a/fichier12

#créer un fichier pour vérifier le droit d'écriture
touch dir_a/fichier14

#supprimer un fichier créé
rm dir_a/fichier14
echo

#afficher le contenu d'un fichier pour vérifier le droit de lecture dans dir_c
cat dir_c/fichier32
echo
echo "Done !"
