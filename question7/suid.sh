#!/bin/bash

#ajout des utilisateurs et des groupes
sudo adduser admin_
sudo adduser lambda_a
sudo adduser lambda_b
sudo addgroup groupe_a 
sudo addgroup groupe_b
sudo addgroup groupe_c

#Creation des différentes répertoires
mkdir dir_a
mkdir dir_b
mkdir dir_c

#attribution des groupes aux différents utilisateurs
sudo adduser lambda_a groupe_a
sudo adduser lambda_b groupe_b
sudo adduser lambda_a groupe_c
sudo adduser lambda_b groupe_c
sudo adduser admin_ groupe_a
sudo adduser admin_ groupe_b
sudo adduser admin_ groupe_c

#changer à la fois le propriétaire et le groupe d'un fichier
sudo chown admin_:groupe_a dir_a
sudo chown admin_:groupe_b dir_b
sudo chown admin_:groupe_c dir_c

#Changement des droits
sudo chmod +t dir_a dir_b dir_c
sudo chmod o-r dir_a dir_b dir_c
sudo chmod g+rwx dir_a dir_b dir_c
sudo chmod g+s dir_a dir_b dir_c
sudo chmod g-w dir_c

