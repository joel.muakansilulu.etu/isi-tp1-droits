#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "check_pass.h"

#define SIZE 32

int main(int argc, char const *argv[])
{

    struct stat st;
    char password[SIZE];

    gid_t gid = getgid();
    uid_t uid = getuid();

    stat(argv[1], &st);

    if (gid != st.st_gid)
    {
        fprintf(stderr, "You do not have the right to modify the file %s\n", argv[1]);
        return EXIT_FAILURE;
    }

    printf("Enter password: ");
    fgets(password, SIZE, stdin);
    printf("%s\n", password);

    if (check_pass("/home/admin/passwd", uid, password))
    {
        int rm = remove(argv[1]);
        if (!rm){
            printf("%s Done!\n", argv[1]);
        }else{
            printf("Deletion failed\n");
        }
    }
    else{
        printf("Bad password !\n");
        return 0;
    }

    return 1;
}
