#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pwd.h>
#include "check_pass.h"

int check_pass(char *filename, uid_t user_id, char *password)
{
    FILE *f;
    char *line = NULL;
    char *pwd;
    char *uid;
    size_t len = 0;

    if ((f = fopen(filename, "r")) != NULL)
    {
        while (getline(&line, &len, f) != -1)
        {
            uid = strtok(line, ":");
            pwd = strtok(NULL, ":");
            if (atoi(uid) == user_id)
            {
                fclose(f);
                return strcmp(pwd, crypt(password, "salt")) == 0 ? 1 : 0;
            }
        }
        fclose(f);
    }
    else
    {
        printf("Failed to open file !");
    }

    if (line)
        free(line);

    return 0;
}
