#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pwd.h>

#define SIZE 32

int main(int argc, char *argv[])
{
    FILE *fsrc, *fdst;
    char *lsrc = NULL;
    char *ldst = NULL;
    char *pwd;
    char *uid;
    char not_found = 1;
    size_t len = 0;
    ssize_t size;
    char new_pwd[SIZE];
    char old_pwd[SIZE];

    uid_t user_id = getuid();

    if ((fsrc = fopen("/home/admin/passwd", "r")) != NULL)
    {
        while (not_found && (size = getline(&lsrc, &len, fsrc)) != -1)
        {
            uid = strtok(lsrc, ":");
            pwd = strtok(NULL, ":");
        
            if (atoi(uid) == user_id)
                not_found = 0;
        }
        fclose(fsrc);
    }

    if (size != -1)
    {
        printf("Enter password: ");
        fgets(old_pwd, SIZE, stdin);
        printf("%s\n", old_pwd);

        if (strcmp(pwd, crypt(old_pwd, "salt")))
        {
            fprintf(stderr, "Bad password\n");
            free(lsrc);
            return EXIT_FAILURE;
        }
        printf("Enter the new password: ");
    }
    else
    {
        printf("Enter password: ");
    }

    fgets(new_pwd, SIZE, stdin);
    printf("%s\n", new_pwd);

    if ((fsrc = fopen("/home/admin/passwd", "r")) != NULL && (fdst = fopen("/home/admin/passwd.tmp", "a")) != NULL)
    {
        while (getline(&ldst, &len, fsrc) != -1)
        {
            char *a = strtok(ldst, ":");
            char *b = strtok(NULL, ":");

            if (atoi(a) != user_id)
                fprintf(fdst, "%s:%s:\n", a, b);
        }
        fprintf(fdst, "%d:%s:\n", user_id, crypt(new_pwd, "salt"));
        free(ldst);
        fclose(fsrc);
        fclose(fdst);
    }

    printf("\n");
    remove("/home/admin/passwd");
    rename("/home/admin/passwd.tmp", "/home/admin/passwd");

    return EXIT_SUCCESS;
}
