# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: Muaka Nsilulu Joel (joel.muakansilulu.etu@univ-lille.fr)

-

## Question 1

Le processus ne peut pas écrire car il a été lancé par toto qui est propriétaire et qui ne dispose pas de droit d'écriture.

## Question 2

* Pour un répertoire également le caractère x indique qu'il est exécutable.

* Apres avoir creer le repertoire mydir avec l'utilisateur ubuntu et avoir retiré le droit d'exécution au groupe ubutnu, on constate que l'utilisateur toto ne peut pas entrer dans le repertoire mydir car étant du groupe ubuntu la permission ne lui ai pas accordé.

* Lorsqu'on execute la commande "ls -al mydir" avec l'utilisateur toto apres avoir creer le fichier data.txt, on ramarque que le fichier apparait avoir son type mais pas ses droits qui restent inconnus. 
Le fichier apparait car toto a le droit de lecture mais pas les droits car il n'a pas le droit d'acceder au repertoire et ses informations.

## Question 3

* Avant d'avoir activé le flag set-user-id du fichier exécutable, on a les ids suivants :

        RUID : 1001
        RGID : 1001
        EUID : 1001
        EGID : 1001

        Le processus n'arrive pas à ouvrir le fichier mydir/mydata.txt en lecture (Cannot open file: Permission denied).

* Apres avoir activé le flag set-user-id du fichier exécutable, on a les ids suivants :

        RUID : 1001
        RGID : 1001
        EUID : 1000
        EGID : 1001

        Le processus arrive finalement à ouvrir le fichier mydir/mydata.txt en lecture.

## Question 4

* Les valeurs des différents ids sont :

        EUID :  1001
        EGID :  1001

* Pour changer les attributs d'un fichier sans demander à son administrateur, il faut recupérer l'id utilisateur de cet administrateur et en faire son effectif id utilisateur.

## Question 5

* La commande **chfn** sert à modifier le nom complet et les informations associées à un utilisateur.

* le fichier /usr/bin/chfn a pour permissions **-rwsr-xr-x**. Le cas particulier est le **rws** de la partie propriétaire. Cela signifie que le programme est autorisé à être exécuté avec l'id effectif du propriétaire lorsqu'il est exécuté par n'importe quel utilisateur.

## Question 6

Les mots de passe des utilisateurs sont stockés dans le fichier **shadow** car dans le fichier passwd les mots de passe sont notés **x** miniscule qui signifie qu'ils sont chiffrés et se trouve dans le fichier shadow.

## Question 7

Afin de tester toutes les descriptions données pour la question 7, on lance les différents script bash dans cet ordre :

* suid.sh : pour créer les ajouter les utilisateurs, les groupes; créer les répertoires; repartir les groupes et les droits de propriété puis gérér les attributions des permissions.
* admin_.sh : lance les tests pour l'admin.
* lambda_a.sh : lance les tests pour lambda_a.
* lambda_b.sh : lance les tests pour lambda_b.

## Question 8

Pour cette question, on peut se servir de la précédente pour créer les utilisateurs, les repertoires puis gérer les permissions. Ensuite on peut lancer le Makefile qui compile les sources et gère les permissions.

## Question 9

Pour cette question, on ajoute aux données de la question précédente le fichier **pwg.c** qqui est une amélioration du fichier **rmg.c**.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








