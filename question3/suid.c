#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char *argv[])
{
    uid_t ur = getuid();
    uid_t gr = getgid();

    uid_t ue = geteuid();
    uid_t ge = getegid();

    printf("\nRUID : %d\n",ur);
    printf("RGID : %d\n",gr);
    printf("EUID : %d\n",ue);
    printf("EGID : %d\n\n",ge);

    FILE *f;
    char c;
    f=fopen("mydir/data.txt","r");

    if (f == NULL) {
        perror("Cannot open file");
        exit(EXIT_FAILURE);
    }

    while((c=fgetc(f))!=EOF){
        printf("%c",c);
    }

    fclose(f);
    exit(EXIT_SUCCESS);

    return 0;
}